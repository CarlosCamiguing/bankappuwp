﻿using BankApp.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace BankingApp
{
    /// <summary>
    /// Main page of the banking application that shows the list of accounts and account details for selected account
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //DONE: define a HAS-A relationship between the page and the bank
        /// <summary>
        /// The bank object holding the accounts managed by the application
        /// </summary>
        private Bank _bank;

        public MainPage()
        {
            this.InitializeComponent();

            //create the bank object which holds the list of account
            _bank = new Bank();

            //fill the bank with a list of accounts with random data
            _bank.CreateAccounts();

            //load the accounts into the UI
            DisplayAccounts();
        }

        /// <summary>
        /// Iterate through all the accounts in the bank and add them to the accounts list view 
        /// </summary>
        private void DisplayAccounts()
        {
            //go through all accounts in the bank and add to list
            foreach (Account acct in _bank.AccountList)
            {
                //Add the account as an item in the list view
                _lstAccounts.Items.Add(acct);
            }
        }

        /// <summary>
        /// Event handler called when the user selects an account in the list view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAccountSelected(object sender, SelectionChangedEventArgs e)
        {
            //Obtain the account selected by the user
            Account selectedAccount = _lstAccounts.SelectedItem as Account;

            //reset the title of the transaction list
            _txtAcctDetails.Text = $"Account Details: {selectedAccount.AccountNumber}";
            //clear the list of transactions of the previously selected account
            _lstTransactions.Items.Clear();

            //DONE: display the list of transactions into the _lstTransaction
            foreach (Transaction trans in selectedAccount.TransactionList)
            {
                _lstTransactions.Items.Add(trans);
            }
        }

		private void OnRemoveAll(object sender, RoutedEventArgs e)
		{
			//re-create the bank to start from scratch with no accounts.
			_bank = new Bank();

			//clear all accounts and trasactions from the UI
			_lstAccounts.Items.Clear();
			_lstTransactions.Items.Clear();

		}

		private void OnDisplayDataStorageLocation(object sender, RoutedEventArgs e)
		{
			string acctDataDir = Path.Combine(ApplicationData.Current.LocalFolder.Path, "BankingData");
			MessageDialog msgDlg = new MessageDialog(acctDataDir, "Banking Data Location");
			msgDlg.ShowAsync();
		}
	}
}
